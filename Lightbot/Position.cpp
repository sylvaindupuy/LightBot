//Copyright 2017 Sylvain Dupuy, Julien Bascouzaraix
/*
This file is part of Lightbot.

Lightbot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lightbot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Lightbot. If not, see <http://www.gnu.org/licenses/>.
*/

//#include "Position.h"
#include "Grid.h"

Position::Position(int line, int col)
    : m_line{line}
    , m_col{col}
{

}

int Position::getLine()const
{
    return m_line;
}

int Position::getCol()const
{
    return m_col;
}

Position Position::next(int dir) const
{
    int line =m_line;
    int col = m_col;
    switch (dir) {
    case 0: // North
        line--;
        break;
    case 1: // North East
        col++;
        if(m_col%2 != 0)
            line--;
        break;
    case 2: // South East
        col++;
        if(m_col%2 == 0)
            line++;
        break;
    case 3: // South
        line++;
        break;
    case 4: // South West
        col--;
        if(m_col%2 == 0)
            line++;
        break;
    case 5: // North West
        if(m_col%2 == 0)
            col--;
        else{
            col--;
            line--;
        }
        break;
    default:
        break;
    }
    return Position{line,col};
}

/* this function return the position in pixels acccording to the given grid */
sf::Vector2f Position::PositionToPixels(const Grid* const g) const
{
    int j=0;
    sf::Vector2f tmp{0,0};
    float radius = g->getRadius();

    // Set the good line
    tmp=sf::Vector2f(g->firstX(),g->firstY()-(m_line*(tmp.y-radius*sin(M_PI/3)-tmp.y)*2));

    // We advance to the right column...
    if(m_col>0)
    {
        for(j=j;j<m_col;j++)
        {
            if(j%2==0){
                tmp=sf::Vector2f(tmp.x+radius+radius*cos(M_PI/3),tmp.y-radius*sin(M_PI/3));
            }else{
                tmp=sf::Vector2f(tmp.x+radius+radius*cos(M_PI/3),tmp.y+radius*sin(M_PI/3));
            }
        }
    }
    else if(m_col<0)
    {
        for(j=j;j<abs(m_col);j++)
        {
            if(j%2==0){
                tmp=sf::Vector2f(tmp.x-radius-radius*cos(M_PI/3),tmp.y-radius*sin(M_PI/3));
            }else{
                tmp=sf::Vector2f(tmp.x-radius-radius*cos(M_PI/3),tmp.y+radius*sin(M_PI/3));
            }
            j++;
        }
    }
    return tmp;
}


bool Position::operator ==(Position const &p) const
{
    return p.getCol()==this->getCol()
            && p.getLine()==this->getLine();
}

bool Position::operator !=(Position const &p) const
{
    return !(p.getCol()==this->getCol()
             && p.getLine()==this->getLine());
}
