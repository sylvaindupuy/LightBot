//Copyright 2017 Sylvain Dupuy, Julien Bascouzaraix
/*
This file is part of Lightbot.

Lightbot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lightbot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Lightbot. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Application.h"

Application::Application()
    :m_place{Place::MAIN_MENU}
    ,m_state{State::INITIAL}
    ,m_program{Program::INITIAL}
    ,m_start{0,0}
    ,m_currentLvl{0}
    ,m_currentLvlEditor{0}
    ,m_confirmName{false}
    ,m_lvl{"Backgrounds/background.png"}
    ,m_mainMenu{"Backgrounds/fond.png"}
    ,m_selectionLvlMenu{"Backgrounds/fondEmpty.png",12}
    ,m_selectionEditorMenu{"Backgrounds/fondEmpty.png",12}
    ,m_menuEnd{"Backgrounds/fin.png"}
    ,m_editor{"Backgrounds/fondEditor.png"}
    ,m_allowed_char{" abcdefghijklmnopqrstuvwxyzéàè!"}
    ,m_buttonPressed{nullptr}
{
    // Filling the vector of level names
    namesFiles("Levels/",m_namesLvl);
    namesFiles("Levels/Editor/",m_namesLvlEditor);
    // load the font
    if(!m_font.loadFromFile("font.ttf"))
        exit(EXIT_FAILURE);
    // Text use in the editor
    m_textName.setFont(m_font);
    m_textName.setColor(sf::Color::Magenta);
    m_textName.setString(m_nameLvl);

    // Adding the buttons (true because they have a sprite Pushed)
    m_mainMenu.addButton(sf::Vector2f{425,350},"play","Tools/play.png",{227,81},true);
    m_mainMenu.addButton(sf::Vector2f{600,500},"editor","Tools/editor.png",{227,81},true);
    m_mainMenu.addButton(sf::Vector2f{790,350},"quit","Tools/quit.png",{227,81},true);

    // Init of "tools" button selection menus
    m_selectionEditorMenu.addButton(sf::Vector2f{150,640},"Nouveau niveau","Tools/newLevel.png",{200,80});

    for(unsigned int i=0;i<m_namesLvlEditor.size();i++)
        m_selectionEditorMenu.addButtonChoice(positionsLevelChoice(m_selectionEditorMenu.getNbNext()),m_namesLvlEditor[i],"Tools/lvl.png",{150,60},false,true);
    for(unsigned int i=0;i<m_namesLvl.size();i++)
        m_selectionLvlMenu.addButtonChoice(positionsLevelChoice(m_selectionLvlMenu.getNbNext()),m_namesLvl[i],"Tools/lvl.png",{150,60},false,true);
}

Application::~Application()
{
}

void Application::mousePressed()
{
    ///////////////////////////////////////////////// Management of buttons /////////////////////////////////////////////////

    switch (m_place) {
    case Place::MAIN_MENU:
        if(m_mainMenu.getButton("play")->contains(m_mouse)){
            m_mainMenu.getButton("play")->isPressed(true);
        }else if(m_mainMenu.getButton("quit")->contains(m_mouse)){
            m_mainMenu.getButton("quit")->isPressed(true);
        }else if(m_mainMenu.getButton("editor")->contains(m_mouse)){
            m_mainMenu.getButton("editor")->isPressed(true);
        }
        break;
    case Place::SELECTION_LVL:
    {
        bool trouve=false;
        int i=0;
        if(m_selectionLvlMenu.getChoiceButton(m_mouse)){
            m_place=Place::GAME;
            std::string action = m_selectionLvlMenu.getChoiceButton(m_mouse)->getAction();
            while(!trouve){
                if(action == m_namesLvl[i])
                    trouve=true;
                else
                    i++;
            }
            m_currentLvl=i;
            m_lvl.loadLvl(m_namesLvl[i]);

        }else if(m_selectionLvlMenu.getNbPages()>1 && m_selectionLvlMenu.getButton("previousPage")->circleContains(m_mouse)){
            if(m_selectionLvlMenu.getCurrentPage() > 1)
                m_selectionLvlMenu.setCurrentPage(m_selectionLvlMenu.getCurrentPage()-1);
        }else if(m_selectionLvlMenu.getNbPages()>1 && m_selectionLvlMenu.getButton("nextPage")->circleContains(m_mouse)){
            if(m_selectionLvlMenu.getCurrentPage() < m_selectionLvlMenu.getNbPages())
                m_selectionLvlMenu.setCurrentPage(m_selectionLvlMenu.getCurrentPage()+1);
        }
    }
        break;
    case Place::SELECTION_EDITOR:
    {
        bool trouve=false;
        int i=0;
        if(m_selectionEditorMenu.getButton("Nouveau niveau")->contains(m_mouse)){
            m_place=Place::EDITOR;
        }else if(m_selectionEditorMenu.getNbPages()>1 && m_selectionEditorMenu.getButton("previousPage")->circleContains(m_mouse)){
            if(m_selectionEditorMenu.getCurrentPage() > 1)
                m_selectionEditorMenu.setCurrentPage(m_selectionEditorMenu.getCurrentPage()-1);
        }else if(m_selectionEditorMenu.getNbPages()>1 && m_selectionEditorMenu.getButton("nextPage")->circleContains(m_mouse)){
            if(m_selectionEditorMenu.getCurrentPage() < m_selectionEditorMenu.getNbPages())
                m_selectionEditorMenu.setCurrentPage(m_selectionEditorMenu.getCurrentPage()+1);
        }else if(m_selectionEditorMenu.getChoiceButton(m_mouse)){
            if(m_button==sf::Mouse::Right){
                std::string action;
                if(m_selectionEditorMenu.erase(m_mouse,action))
                    m_namesLvlEditor.erase(std::find(m_namesLvlEditor.begin(),m_namesLvlEditor.end(),action));
            }else{
                m_place=Place::GAME_EDITOR;
                std::string action = m_selectionEditorMenu.getChoiceButton(m_mouse)->getAction();
                while(!trouve){
                    if(action == m_namesLvlEditor[i])
                        trouve=true;
                    else
                        i++;
                }
                m_currentLvlEditor=i;
                m_lvl.loadLvl("Editor/"+m_namesLvlEditor[i]);
            }
        }
    }
        break;
    case Place::EDITOR:
        if(m_editor.getButton("colMinus")->circleContains(m_mouse)){
            m_editor.changeCols(-1);
        }else if(m_editor.getButton("colPlus")->circleContains(m_mouse)){
            m_editor.changeCols(1);
        }else if(m_editor.getButton("lineMinus")->circleContains(m_mouse)){
            m_editor.changeLines(-1);
        }else if(m_editor.getButton("linePlus")->circleContains(m_mouse)){
            m_editor.changeLines(1);
        }else if(m_editor.getButton("mainMinus")->circleContains(m_mouse)){
            m_editor.changeMaxs(0);
        }else if(m_editor.getButton("mainPlus")->circleContains(m_mouse)){
            m_editor.changeMaxs(1);
        }else if(m_editor.getButton("proc1Minus")->circleContains(m_mouse)){
            m_editor.changeMaxs(2);
        }else if(m_editor.getButton("proc1Plus")->circleContains(m_mouse)){
            m_editor.changeMaxs(3);
        }else if(m_editor.getButton("proc2Minus")->circleContains(m_mouse)){
            m_editor.changeMaxs(4);
        }else if(m_editor.getButton("proc2Plus")->circleContains(m_mouse)){
            m_editor.changeMaxs(5);
        }else if(m_editor.getButton("Confirmation")->contains(m_mouse)){
            if(m_editor.canConfirm()){
                m_lvl.loadLvl(m_editor.getGrid(),m_editor.getRobot(),m_editor.getMaxMain(),m_editor.getMaxProc1(),m_editor.getMaxProc2());
                m_place=Place::CONFIRM;
            }
        }else if(m_editor.getButton("mainMenu")->contains(m_mouse)){
            m_editor.clearEditor();
            m_place=Place::MAIN_MENU;
        }else if(m_editor.getRobot()->contains(m_mouse)){
            // Right click
            if(m_button==sf::Mouse::Right)
                m_editor.getRobot()->Rotation();
            else
                m_state=State::ROBOT;
        }else
            m_editor.changeCase(m_editor.mouseToHex(m_mouse));
        break;
    case Place::END_MENU:
        if(m_menuEnd.getButton("quit")->contains(m_mouse)){
            m_running=false;
        }
        break;
    case Place::GAME:
    case Place::CONFIRM:
    case Place::GAME_EDITOR:
    {
        if(m_lvl.getButton("stepbystep")->circleContains(m_mouse)){
            if(m_program==Program::INITIAL){
                m_lvl.initProgram();
                if(m_lvl.getProgSize() > 0)
                    m_program=Program::STEP;
            }else if(m_program==Program::PAUSE)
                m_program=Program::STEP;
        }
        else if(m_lvl.getButton("play")->circleContains(m_mouse))
        {
            if(m_program==Program::INITIAL){
                m_lvl.initProgram();
                if(m_lvl.getProgSize() > 0)
                    m_program=Program::RUN;
            }
        }
        else if(m_lvl.getButton("clear")->circleContains(m_mouse))
        {
            if(m_program==Program::INITIAL)
                m_lvl.clearInstructions();
        }
        else if(m_lvl.getButton("tryagain")->circleContains(m_mouse))
        {
            if(m_program!=Program::INITIAL && !m_lvl.win()){
                switch (m_place) {
                case Place::CONFIRM:
                    m_lvl.loadLvl(m_editor.getGrid(),m_editor.getRobot(),m_editor.getMaxMain(),m_editor.getMaxProc1(),m_editor.getMaxProc2());
                    break;
                case Place::GAME:
                    m_lvl.loadLvl(m_namesLvl[m_currentLvl]);
                    break;
                case Place::GAME_EDITOR:
                    m_lvl.loadLvl("Editor/"+m_namesLvlEditor[m_currentLvlEditor]);
                    break;
                default:
                    break;
                }
                m_program=Program::INITIAL;
            }
        }
    }

        if(m_program!=Program::RUN && m_program!=Program::STEP && !m_confirmName){
            if(m_lvl.getButton("mainMenu")->contains(m_mouse))
            {
                initLvl();
                m_place=Place::MAIN_MENU;
            }
        }
        if(m_program==Program::FINAL && m_lvl.win()){         // program end
            if(m_lvl.getButton("next")->contains(m_mouse)){
                if(m_place==Place::GAME_EDITOR){
                    m_currentLvlEditor++;
                    if(m_currentLvlEditor < m_namesLvlEditor.size()){
                        m_lvl.loadLvl("Editor/"+m_namesLvlEditor[m_currentLvlEditor]);
                        initLvl();
                    }else{
                        initLvl();
                        m_place=Place::END_MENU;
                    }
                }else if(m_place==Place::GAME){
                    m_currentLvl++;
                    if(m_currentLvl<m_namesLvl.size()){
                        m_lvl.loadLvl(m_namesLvl[m_currentLvl]);
                        initLvl();
                    }else if(m_currentLvl==m_namesLvl.size()){
                        m_place=Place::END_MENU;
                        initLvl();
                    }
                }
            }else if(m_lvl.getButton("previous")->contains(m_mouse)){
                if(m_place==Place::GAME_EDITOR){
                    if(m_currentLvlEditor > 0){
                        m_currentLvlEditor--;
                        m_lvl.loadLvl("Editor/"+m_namesLvlEditor[m_currentLvlEditor]);
                        initLvl();
                    }
                }else if(m_place==Place::GAME){
                    if(m_currentLvl > 1){
                        m_currentLvl--;
                        m_lvl.loadLvl(m_namesLvl[m_currentLvl]);
                        initLvl();
                    }
                }
            }
        }

        ///////////////////////////////////////////////// Management of Actions /////////////////////////////////////////////////

        if(m_program==Program::INITIAL){
            if(m_lvl.contains(m_mouse,-1))                       // if in Orders
            {
                m_buttonTmp = Button(*m_lvl.contains(m_mouse,-1)); // create a tmp button
                m_start = m_mouse;
                m_state = State::ADDING;
            }
            else if(m_lvl.contains(m_mouse,0))                    // if in Main
            {
                m_buttonPressed = m_lvl.contains(m_mouse,0);      // store button pressed
                m_lvl.eraseFrom(m_buttonPressed,0);
                m_start = m_mouse;
                m_state = State::MOUVEMENT;
            }
            else if(m_lvl.contains(m_mouse,1))                    // if in Proc1
            {
                m_buttonPressed = m_lvl.contains(m_mouse,1);
                m_lvl.eraseFrom(m_buttonPressed,1);
                m_start = m_mouse;
                m_state = State::MOUVEMENT;
            }
            else if(m_lvl.contains(m_mouse,2))                    // if in Proc2
            {
                m_buttonPressed = m_lvl.contains(m_mouse,2);
                m_lvl.eraseFrom(m_buttonPressed,2);
                m_start = m_mouse;
                m_state = State::MOUVEMENT;
            }
        }
        break;
    default:
        break;
    }
}

void Application::mouseReleased()
{
    ///////////////////////////////////////////////// Management of buttons /////////////////////////////////////////////////

    if(m_place==Place::EDITOR){
        if(m_state==State::ROBOT){
            if(m_editor.contains(m_editor.mouseToHex(m_mouse))){
                m_editor.getRobot()->setPosition(m_editor.mouseToHex(m_mouse));
                m_state=State::INITIAL;
            }else{
                m_editor.getRobot()->setPosition(m_editor.getRobot()->getPosition());
                m_state=State::INITIAL;
            }
        }
    }

    if(m_place==Place::MAIN_MENU){            // Main menu
        if(m_mainMenu.getButton("play")->contains(m_mouse) && m_mainMenu.getButton("play")->isPressed()){
            m_place=Place::SELECTION_LVL;
        }else if(m_mainMenu.getButton("quit")->contains(m_mouse) && m_mainMenu.getButton("quit")->isPressed()){
            m_running = false;
        }else if(m_mainMenu.getButton("editor")->contains(m_mouse) && m_mainMenu.getButton("editor")->isPressed()){
            m_place=Place::SELECTION_EDITOR;
        }
        m_mainMenu.getButton("play")->isPressed(false);
        m_mainMenu.getButton("quit")->isPressed(false);
        m_mainMenu.getButton("editor")->isPressed(false);

        ///////////////////////////////////////////////// Management of Actions /////////////////////////////////////////////////

    }else if( inGame() && m_program==Program::INITIAL){           // if in Game

        switch (m_state) {
        case State::ADDING:          // if button pressed in orders

            // Adding button
            if(m_lvl.in(m_mouse,0,m_buttonTmp.getAction()))        // if released on main
            {
                m_lvl.addButton(new Button{m_buttonTmp},m_mouse,0);
            }
            else if(m_lvl.in(m_mouse,1,m_buttonTmp.getAction()))  // if released on proc1
            {
                m_lvl.addButton(new Button{m_buttonTmp},m_mouse,1);
            }
            else if(m_lvl.in(m_mouse,2,m_buttonTmp.getAction()))  // if released on proc2
            {
                m_lvl.addButton(new Button{m_buttonTmp},m_mouse,2);
            }
            break;
        case State::MOUVEMENT:    // if button pressed in main/proc1/proc2

            // Adding button
            if(m_lvl.in(m_mouse,0,m_buttonPressed->getAction()))            // adding in main
            {
                m_lvl.addButton(m_buttonPressed,m_mouse,0);
            }
            else if(m_lvl.in(m_mouse,1,m_buttonPressed->getAction()))  // adding in proc1
            {
                m_lvl.addButton(m_buttonPressed,m_mouse,1);
            }
            else if(m_lvl.in(m_mouse,2,m_buttonPressed->getAction()))  // adding in proc2
            {
                m_lvl.addButton(m_buttonPressed,m_mouse,2);
            }
            else  // if no adding button, we delete it
                m_lvl.deleteButton(m_buttonPressed);
            break;
        default:
            break;
        }
        m_state=State::INITIAL;
    }
}

void Application::mouseMoved()    // Move with release button
{
}

void Application::mouseDragged()  // Move with pressed button
{
    if(inGame()){
        switch (m_state) {
        case State::ADDING:
            m_offset = m_mouse-m_start;
            m_buttonTmp.setPosition({m_buttonTmp.getPosition().x + m_offset.x
                                     ,m_buttonTmp.getPosition().y + m_offset.y});
            m_start = m_mouse;
            break;
        case State::MOUVEMENT:
            m_offset = m_mouse-m_start;
            m_buttonPressed->setPosition({m_buttonPressed->getPosition().x + m_offset.x
                                          ,m_buttonPressed->getPosition().y + m_offset.y});
            m_start = m_mouse;
        default:
            break;
        }
    }else if(m_place==Place::EDITOR && m_state==State::ROBOT)
        m_editor.getRobot()->setPosition(m_mouse);
}

bool Application::inGame() const
{
    return m_place==Place::GAME || m_place==Place::CONFIRM || m_place==Place::GAME_EDITOR;
}

void Application::initLvl()
{
    m_lvl.clearInstructions();
    m_program=Program::INITIAL;
}

void Application::initName()
{
    m_confirmName=false;
    m_nameLvl="";
    m_textName.setString(m_nameLvl);
}

void Application::key_pressed(sf::Event::KeyEvent event)
{
    switch (event.code) {
    case sf::Keyboard::Q:
        if(!(m_place == Place::CONFIRM))
            stop();
        break;
    case sf::Keyboard::Escape:                     // Escape
        if(m_program!=Program::STEP && m_program!=Program::RUN){
            switch (m_place) {
            case Place::GAME:
                initLvl();
                m_place=Place::SELECTION_LVL;
                break;
            case Place::CONFIRM:
                initLvl();
                initName();
                m_place=Place::EDITOR;
                break;
            case Place::GAME_EDITOR:
            case Place::EDITOR:
                initLvl();
                m_editor.clearEditor();
                m_place=Place::SELECTION_EDITOR;
                break;
            case Place::SELECTION_EDITOR:
            case Place::SELECTION_LVL:
            case Place::END_MENU:
                m_place=Place::MAIN_MENU;
            default:
                break;
            }
        }
        break;
    case sf::Keyboard::Return:                     // Enter (or return)
        if(event.code == sf::Keyboard::Return && m_confirmName && m_nameLvl.size()>0){
            if(std::find(m_namesLvlEditor.begin(),m_namesLvlEditor.end(),m_nameLvl) == m_namesLvlEditor.end()){
                m_namesLvlEditor.push_back(m_nameLvl);
                m_lvl.loadLvl(m_editor.getGrid(),m_editor.getRobot(),m_editor.getMaxMain(),m_editor.getMaxProc1(),m_editor.getMaxProc2());
                m_lvl.storeLevel(m_nameLvl);
                m_selectionEditorMenu.addButtonChoice(positionsLevelChoice(m_selectionEditorMenu.getNbNext()),m_nameLvl,"Tools/lvl.png",{150,60},false,true);
                initName();
                initLvl();
                m_place=Place::SELECTION_EDITOR;
            }else{
                std::cout << "Le niveau " << m_nameLvl << " existe déjà." << std::endl;
                m_nameLvl="";
                m_textName.setString(m_nameLvl);
            }
        }
        break;
    case sf::Keyboard::BackSpace:
        if(m_confirmName && m_nameLvl.size()>0){
            m_nameLvl.resize(m_nameLvl.size()-1);
            m_textName.setString(m_nameLvl);
        }
        break;
    default:
        break;
    }
}

void Application::text_entered(sf::Event event)
{
    if(m_confirmName && m_nameLvl.size()<20){
        // Handle ASCII characters only
        if (event.text.unicode < 128)
        {;
            char tmp = static_cast<char>(event.text.unicode);
            if (m_allowed_char.find(tolower(tmp)) <= m_allowed_char.size())
                m_nameLvl += tmp;
            m_textName.setString(m_nameLvl);
            update();
        }
    }
}

void Application::level()
{
    switch (m_program) {
    case Program::RUN:
        m_lvl.runProgram();
        if(m_lvl.programEnd())
            m_program=Program::FINAL;
        break;
    case Program::STEP:
        m_lvl.runProgram();
        if(m_lvl.programEnd())
            m_program=Program::FINAL;
        else if(m_lvl.animationEnd())
            m_program=Program::PAUSE;
        break;
    default:
        break;
    }

    if(m_program==Program::FINAL)
        if(m_place==Place::CONFIRM && m_lvl.win()){
            m_confirmName=true;
        }
}

void Application::draw()
{
    m_window.clear();

    switch (m_place) {
    case Place::MAIN_MENU:
        m_mainMenu.drawMenu(m_window);
        break;
    case Place::SELECTION_LVL:
        m_selectionLvlMenu.drawSelectionMenu(m_window);
        break;
    case Place::SELECTION_EDITOR:
        m_selectionEditorMenu.drawSelectionMenu(m_window);
        break;
    case Place::GAME:
    case Place::CONFIRM:
    case Place::GAME_EDITOR:
        // Draw the Level (need to know if the level is finished, if we draw the Rect and if its a 'confirm level')
        m_lvl.drawLvl(m_window,m_place==Place::CONFIRM,m_program==Program::FINAL,m_state==State::ADDING or m_state==State::MOUVEMENT,m_mouse);
        // if we are adding a button, we draw it
        if(m_state==State::ADDING)
            m_buttonTmp.drawButton(m_window);
        // if we are moving a button, we draw it
        else if(m_state==State::MOUVEMENT)
            m_buttonPressed->drawButton(m_window);
        // If we are confirm the name
        if(m_confirmName)
            m_window.draw(m_textName);
        break;
    case Place::EDITOR:
        m_editor.drawEditor(m_window);
        break;
    case Place::END_MENU:
        m_menuEnd.drawMenu(m_window);
        break;
    default:
        break;
    }

    m_window.display();
}

void Application::run()
{
    m_window.create( sf::VideoMode{WINDOW_X,WINDOW_Y, 32},
                     "Lightbot",sf::Style::Close );
    m_window.setFramerateLimit(60);

    m_button_pressed = false;
    m_running = true;
    while (m_running) {
        if(inGame()){
            level();
        }
        process_events();
        draw();
    }
}


void Application::stop() {
    m_running = false;
}

void Application::update()
{
    sf::Vector2f pos;
    pos.x = WINDOW_X/2 - (m_textName.getGlobalBounds().width) / 2;
    pos.y = WINDOW_Y/2 - (m_textName.getGlobalBounds().height) / 2+m_textName.getPosition().y -m_textName.getGlobalBounds().top;
    m_textName.setPosition(pos);
}

void Application::process_events()
{
    if (! m_window.isOpen()) {
        stop();
        return;
    }

    for (sf::Event event; m_window.pollEvent(event); )
    {
        switch (event.type)
        {
        case sf::Event::Closed :
            stop();
            break;
        case sf::Event::TextEntered :
            text_entered(event);
            break;
        case sf::Event::KeyPressed :
            key_pressed(event.key);
            break;
        case sf::Event::MouseButtonPressed :
            m_button_pressed = true;
            m_button = event.mouseButton.button;
            mousePressed();
            break;
        case sf::Event::MouseButtonReleased :
            m_button_pressed = false;
            m_button = event.mouseButton.button;
            mouseReleased();
            break;
        case sf::Event::MouseMoved :
        {
            // conversion des coordonnées "pixel" en coordonnées "fenetre"
            // au cas où on redimensionne.
            auto pos = m_window.mapPixelToCoords( {event.mouseMove.x,
                                                   event.mouseMove.y });
            m_mouse.x = pos.x;
            m_mouse.y = pos.y;
        }
            if (m_button_pressed) {
                mouseDragged();
            } else {
                mouseMoved();
            }
            break;
        default:
            break;
        }
    }
}
