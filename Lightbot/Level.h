#ifndef LEVEL_H
#define LEVEL_H

#include "Menu.h"

class Level : public Menu
{
private:
    unsigned int m_maxMain,m_maxProc1,m_maxProc2;

    // Text variables
    sf::Text m_text;
    sf::Font m_font;

    // Sprites and textures
    std::string m_nameLvl;
    sf::Sprite m_endLvl;
    sf::Texture m_endLvlTexture;

    // Used to display the possible position of addition
    sf::RectangleShape m_rect;
    // Used if you need to pressed the button try again
    sf::CircleShape    m_circle;

    // The differents vectors
    std::vector<Button*> m_orders;  // Here, this this action (moving forward, rotate ...)
    std::vector<Button*> m_main;    //                  ``
    std::vector<Button*> m_proc1;   //                  ``
    std::vector<Button*> m_proc2;   //                  ``

    // A grid and a Robot
    Grid m_grid;
    Robot m_robot;

    // Had a button in orders
    void addButtonOrders(std::string const &action,std::string const &nameTexture);

    void drawRect(sf::Vector2f const &mouse, sf::RenderWindow & window);
public:
    // Constructor
    Level(std::string const &background);
    // Management of the movements of the "actions"
    void addButton(Button *b,sf::Vector2f const &mouse, unsigned int a);
    void deleteButton(Button *b);
    void eraseFrom(Button* b,unsigned int a);
    void updatePositions(unsigned int a, unsigned int i=0);
    bool in(sf::Vector2f const &pos, unsigned int a, std::string const & action)const;
    bool between(sf::Vector2f const &mouse, unsigned int &i, unsigned int a);

    // Load/store a level
    void loadLvl(std::string nameLvl);
    void loadLvl(Grid *grid, Robot *robot, unsigned int maxMain, unsigned int maxProc1, unsigned int maxProc2);
    void storeLevel(std::string const& name);

    // contains and getter
    Button* contains(sf::Vector2f const &pos,int a);
    unsigned getProgSize()const;

    // Management of the Robot and the Program
    void initProgram(int i=0);
    void runProgram();
    bool animationEnd() const;
    bool programEnd() const;

    // Others
    bool win() const;
    void clearInstructions();
    void drawLvl(sf::RenderWindow & window, bool inConfirm, bool inFinal, bool Rect, sf::Vector2f const & m);
    ~Level();
};

#endif // LEVEL_H
