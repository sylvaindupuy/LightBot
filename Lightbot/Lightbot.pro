TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++11 -pedantic -Wall -Wextra
SOURCES += main.cpp \
    Application.cpp \
    Position.cpp \
    Grid.cpp \
    Level.cpp \
    Menu.cpp \
    Button.cpp \
    Robot.cpp \
    Editor.cpp \
    tools.cpp \
    MenuSelection.cpp

# Linux

LIBS += -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system

# Windows

#LIBS += -LC:/Libraries/SFML/lib

#CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-main -lsfml-network -lsfml-window -lsfml-system
#CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-graphics-d -lsfml-main-d -lsfml-network-d -lsfml-window-d -lsfml-system-d

#INCLUDEPATH += C:/Libraries/SFML/include
#DEPENDPATH += C:/Libraries/SFML/include

# fin

HEADERS += \
    Application.h \
    Position.h \
    Grid.h \
    Level.h \
    Menu.h \
    Button.h \
    Robot.h \
    Editor.h \
    tools.h \
    MenuSelection.h
