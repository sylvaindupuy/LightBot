#ifndef EDITOR_H
#define EDITOR_H

#include "Menu.h"

class Editor : public Menu // The class inherits of 'Menu'
{
private:
    // Text variables
    sf::Text m_textMax;
    sf::Font m_font;

    unsigned int m_maxMain=10,m_maxProc1=10,m_maxProc2=10; // Number max of actions

    // Others
    Grid m_grid;
    Robot m_robot;
public:
    // Constructor
    Editor(std::string background);
    // Getters
    Grid *getGrid();
    Robot *getRobot();
    unsigned int getMaxMain()const;
    unsigned int getMaxProc1()const;
    unsigned int getMaxProc2()const;
    bool canConfirm();
    // Setters
    void changeMaxs(unsigned int i);
    void changeCase(Position const & p);
    void changeLines(int i);
    void changeCols(int i);
    void clearEditor();
    //Others
    bool contains(Position const & pos)const;
    Position mouseToHex(sf::Vector2f const &m) const;
    void drawEditor(sf::RenderWindow & window);
};

#endif // EDITOR_H
