#include "tools.h"

/* Replaces in a given word the word wanted by another word */
void replace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos != std::string::npos)
        str.replace(start_pos, from.length(), to);
}

/* Returns the position (center) of the actions in terms of the 'i' and Main/Proc1/Proc2 */
sf::Vector2f positionsActions(unsigned int i, unsigned int a)
{
    sf::Vector2f pos{ORDERS_WIDTH/2,ORDERS_HEIGHT/2};
    int x,y;
    if(a==0){
        x = MAIN_X;
        y = MAIN_Y;
    }else if(a==1){
        x = PROC1_X;
        y = PROC1_Y;
    }else{
        x = PROC2_X;
        y = PROC2_Y;
    }
    return pos += sf::Vector2f( x + ORDERS_SPACING + (i%ACTIONS_BY_LINE)*( ORDERS_WIDTH + ORDERS_SPACING ),
                                y + ORDERS_SPACING + (i/ACTIONS_BY_LINE)*( ORDERS_HEIGHT + ORDERS_SPACING ) );
}

/* Return the position of the rect who show the possible position of addition */
sf::Vector2f positionsRectBetween(unsigned int i, unsigned int a)
{
    sf::Vector2f pos = positionsActions(i,a);
    return {pos.x-ORDERS_WIDTH/2-ORDERS_SPACING/2
                ,pos.y};
}

/* return position of Buttons for the selection's menus in terms of the 'i' */
sf::Vector2f positionsLevelChoice(unsigned int i)
{
    return { 330 + (float)(i%4)*( 150 + 50 ),
                200 + (float)(i/4)*( 60 + 50 )};
}

/* Fill a vector with the name of all present files in the given directory */
void namesFiles(std::string const &repertoire, std::vector<std::string> &fics)
{
    struct dirent *fichier;
    DIR *rep;
    std::string name;
    rep = opendir(repertoire.c_str());
    if (rep!=NULL)
    {
        while ((fichier=readdir(rep)))
        {
            name = fichier->d_name;
            replace(name,".txt","");
#ifdef WIN32
            if (!(!strcmp(fichier->d_name,".") || !strcmp(fichier->d_name,"..") || !strcmp(fichier->d_name,"Editor")))
                fics.push_back(name);
#endif
#ifndef WIN32
            if (fichier->d_type == DT_REG)
                fics.push_back(name);

#endif
        }
        closedir(rep);
    }
    std::sort(fics.begin(),fics.end());
}

/* Returns the distance² between the 2 given points */
float square_distance(const sf::Vector2f &p1,
                      const sf::Vector2f &p2)
{
    auto d = p1 - p2;
    return d.x*d.x + d.y*d.y;
}
