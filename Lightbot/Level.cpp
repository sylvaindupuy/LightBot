//Copyright 2017 Sylvain Dupuy, Julien Bascouzaraix
/*
This file is part of Lightbot.

Lightbot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lightbot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Lightbot. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Level.h"

Level::Level(std::string const & background)
    :Menu(background)
    ,m_grid{0,0,{GRIDX_GAME,GRIDY_GAME},{GRID_LVL_WIDTH,GRID_LVL_HEIGHT}}
    ,m_robot{&m_grid,Position(0,0),"Tools/Robot.png"}
{
    if (!m_endLvlTexture.loadFromFile("./Images/Tools/endLvl.png"))
        std::cerr << "cant open the image." <<std::endl;
    if (!m_font.loadFromFile("font.ttf"))
        std::cerr << "Please put the font.ttf file in the compilation directory" << std::endl;

    // Set the sprite used when the Level is win
    m_endLvl.setTexture(m_endLvlTexture);
    m_endLvl.setScale(sf::Vector2f(400/m_endLvl.getLocalBounds().width,150/m_endLvl.getLocalBounds().height));
    m_endLvl.setOrigin(sf::Vector2f(m_endLvl.getLocalBounds().width/2,m_endLvl.getLocalBounds().height/2));
    m_endLvl.setPosition({WINDOW_X/2,WINDOW_Y/2});

    // Set the rect used to display the possible position of addition
    m_rect.setFillColor(sf::Color::Green);
    m_rect.setSize({ORDERS_SPACING/2.5,ORDERS_HEIGHT});
    m_rect.setOrigin(sf::Vector2f(m_rect.getSize().x/2,m_rect.getSize().y/2));
    m_circle.setOutlineColor(sf::Color::Red);
    m_circle.setFillColor(sf::Color::Transparent);
    m_circle.setOutlineThickness(4);
    m_circle.setRadius(22);
    m_circle.setOrigin({25,25});
    m_circle.setPosition({253,38});

    // Set the text who is used to display the max number of actions
    m_text.setFont(m_font);
    m_text.setScale({0.6,0.6});
    m_text.setOrigin(sf::Vector2f(m_text.getLocalBounds().width/2,m_text.getLocalBounds().height/2));
    m_text.setColor(sf::Color(138,160,225));
    m_text.setStyle(sf::Text::Bold);

    // Add all the buttons we need
    addButtonOrders("A","Orders/up.png");
    addButtonOrders("G","Orders/left.png");
    addButtonOrders("D","Orders/right.png");
    addButtonOrders("L","Orders/light.png");
    addButtonOrders("1","Orders/P1.png");
    addButtonOrders("2","Orders/P2.png");
    Menu::addButton(sf::Vector2f(50,35),"play","Tools/playButton.png",sf::Vector2f(50,50));
    Menu::addButton(sf::Vector2f(150,35),"stepbystep","Tools/stepByStep.png",sf::Vector2f(50,50));
    Menu::addButton(sf::Vector2f(250,35),"tryagain","Tools/tryagain.png",sf::Vector2f(50,50));
    Menu::addButton(sf::Vector2f(WINDOW_X/2+85,WINDOW_Y/2),"next","Tools/nextLevel.png",sf::Vector2f(150,100));
    Menu::addButton(sf::Vector2f(WINDOW_X/2-85,WINDOW_Y/2),"previous","Tools/previousLevel.png",sf::Vector2f(150,100));
    Menu::addButton(sf::Vector2f(390,35),"clear","Tools/trash.png",sf::Vector2f(50,50));
    Menu::addButton(sf::Vector2f(1155,670),"mainMenu","Tools/mainMenu.png",sf::Vector2f(190,70));
}

void Level::addButtonOrders(std::string const &action,std::string const &nomTexture)
{
    // Giving the good position and pushBack
    sf::Vector2f pos{ORDERS_X+m_orders.size()*ORDERS_WIDTH+30*m_orders.size(),ORDERS_Y};
    Button* b = new Button{pos,action,nomTexture,sf::Vector2f(ORDERS_WIDTH,ORDERS_HEIGHT)};
    m_orders.push_back(b);
}

/* return a Button* if in mouse then return nullptr */
Button* Level::contains(sf::Vector2f const &pos, int a)
{
    Button* b;
    switch (a) {
    case -1:    // search in orders
    {
        auto contient_position = [& pos] (const Button * ptr) {
            return ptr->contains(pos);
        };
        auto it = std::find_if(m_orders.begin(), m_orders.end(), contient_position);

        b = it == m_orders.end() ? nullptr : *it;
    }
        break;
    case 0:    // search in main
    {
        auto contient_position = [& pos] (const Button * ptr) {
            return ptr->contains(pos);
        };
        auto it = std::find_if(m_main.begin(), m_main.end(), contient_position);

        b = it == m_main.end() ? nullptr : *it;
    }
        break;
    case 1:    // search in proc1
    {
        auto contient_position = [& pos] (const Button * ptr) {
            return ptr->contains(pos);
        };
        auto it = std::find_if(m_proc1.begin(), m_proc1.end(), contient_position);

        b = it == m_proc1.end() ? nullptr : *it;
    }
        break;
    case 2:    // search in proc2
    {
        auto contient_position = [& pos] (const Button * ptr) {
            return ptr->contains(pos);
        };
        auto it = std::find_if(m_proc2.begin(), m_proc2.end(), contient_position);

        b = it == m_proc2.end() ? nullptr : *it;
    }
        break;
    default:
        break;
    }
    return b;
}

unsigned Level::getProgSize() const
{
    return m_robot.getProgSize();
}

/* return true if the mouse is in Main/proc1/proc2 and the maxSize is not reached */
bool Level::in(sf::Vector2f const &pos, unsigned int a,std::string const &action) const
{
    bool in;
    switch (a) {
    case 0:     // search in main
        in = (pos.x>MAIN_X && pos.y>MAIN_Y && pos.x<MAIN_X+MAIN_HEIGHT && pos.y<MAIN_Y+MAIN_WIDTH) && m_main.size()<m_maxMain;
        break;
    case 1:     // search in proc1
    {
        if(action==std::to_string(a)) return false;
        in = (pos.x>PROC1_X && pos.y>PROC1_Y && pos.x<PROC1_X+PROC1_HEIGHT && pos.y<PROC1_Y+PROC1_WIDTH) && m_proc1.size()<m_maxProc1;

        auto contientp1 = [](const Button * ptr) {
            return ptr->getAction()=="1";
        };

        if(in && action=="2"){
            auto it = std::find_if(m_proc2.begin(),m_proc2.end(),contientp1);
            in = it == m_proc2.end() ? true : false;
        }
    }
        break;
    case 2:     // search in proc2
    {
        if(action==std::to_string(a)) return false;
        in = (pos.x>PROC2_X && pos.y>PROC2_Y && pos.x<PROC2_X+PROC1_HEIGHT && pos.y<PROC2_Y+PROC1_WIDTH) && m_proc2.size()<m_maxProc2;

        auto contientp2 = [](const Button * ptr) {
            return ptr->getAction()=="2";
        };

        if(in && action=="1"){
            auto it =  std::find_if(m_proc1.begin(),m_proc1.end(),contientp2);
            in = it == m_proc1.end() ? true : false;
        }
    }
        break;
    default:
        break;
    }
    return in;
}

/* delete the Button* given */
void Level::deleteButton(Button* b)
{
    delete b;
}

/* erase the Button* from the vector (dont delete it) */
void Level::eraseFrom(Button *b, unsigned int a)
{
    std::vector<Button*>::iterator it;
    switch (a) {
    case 0:    // main
        it = std::find(m_main.begin(),m_main.end(),b);
        if (it != m_main.end()){
            m_main.erase(it);
            updatePositions(0);
        }
        break;
    case 1:    // proc1
        it = std::find(m_proc1.begin(),m_proc1.end(),b);
        if (it != m_proc1.end()){
            m_proc1.erase(it);
            updatePositions(1);
        }
        break;
    case 2:    // proc2
        it = std::find(m_proc2.begin(),m_proc2.end(),b);
        if (it != m_proc2.end()){
            m_proc2.erase(it);
            updatePositions(2);
        }
        break;
    default:
        break;
    }
    updatePositions(a);
}

/* The position of the buttons depends on their positions in the vector */
/*        so, this function update the positions of the buttons         */
void Level::updatePositions(unsigned int a, unsigned int i)
{
    // Position update according to the position in the vector
    switch (a) {
    case 0:
        for(i=i;i<m_main.size();i++)
            m_main[i]->setPosition(positionsActions(i,0));
        break;
    case 1:
        for(i=i;i<m_proc1.size();i++)
            m_proc1[i]->setPosition(positionsActions(i,1));
        break;
    case 2:
        for(i=i;i<m_proc2.size();i++)
            m_proc2[i]->setPosition(positionsActions(i,2));
        break;
    default:
        break;
    }
}

/* check if the mouse is between action and 'return' the position (i) */
bool Level::between(sf::Vector2f const &mouse, unsigned int &i, unsigned int a)
{
    i=0;
    int size,j;
    sf::Vector2f pos;
    sf::Vector2f pos2;
    bool between=false;

    if(a==0)
        size = m_main.size();
    else if(a==1)
        size = m_proc1.size();
    else
        size = m_proc2.size();

    while(!between && i<ACTIONS_BY_LINE){
        pos = positionsActions(i,a);
        pos2 = positionsActions(i-1,a);
        if(i==0)
            pos2 = {pos.x-(ORDERS_WIDTH/2+ORDERS_SPACING),pos.y};
        if(mouse.x < pos.x && mouse.x > pos2.x){
            j=i;
            while(!between && j <= size){
                if(mouse.y > positionsActions(j,a).y-ORDERS_HEIGHT/2 && mouse.y < positionsActions(j,a).y+ORDERS_HEIGHT/2)
                    between = true;
                else
                    j+=ACTIONS_BY_LINE;
            }
        }
        i++;
    }
    i=j;

    return between;
}

/* Check if between and add a button in Main/Proc1/Proc2 */
void Level::addButton(Button *b,sf::Vector2f const &mouse, unsigned int a)
{
    unsigned int i;
    switch (a) {
    case 0:
        if(between(mouse,i,0)){
            m_main.insert(m_main.begin()+i,b);
            updatePositions(0,i);
        }else{
            m_main.push_back(b);
            updatePositions(0,m_main.size()-1);
        }
        break;
    case 1:
        if(between(mouse,i,1)){
            m_proc1.insert(m_proc1.begin()+i,b);
            updatePositions(1,i);
        }else{
            m_proc1.push_back(b);
            updatePositions(1,m_proc1.size()-1);
        }
        break;
    case 2:
        if(between(mouse,i,2)){
            m_proc2.insert(m_proc2.begin()+i,b);
            updatePositions(2,i);
        }else{
            m_proc2.push_back(b);
            updatePositions(2,m_proc2.size()-1);
        }
        break;
    default:
        break;
    }

}

/* if the mouse is between actions, drawing the rect */
void Level::drawRect(sf::Vector2f const &mouse,sf::RenderWindow & window){
    unsigned int i;
    if(between(mouse,i,0))
    {
        m_rect.setPosition(positionsRectBetween(i,0));
        window.draw(m_rect);
    }
    else if(between(mouse,i,1))
    {
        m_rect.setPosition(positionsRectBetween(i,1));
        window.draw(m_rect);
    }
    else if(between(mouse,i,2))
    {
        m_rect.setPosition(positionsRectBetween(i,2));
        window.draw(m_rect);
    }
}

/* Load a level according to the name given */
void Level::loadLvl(std::string nameLvl)
{
    m_robot.initRotation();
    m_robot.clearProgram();
    int tmp,tmp2,direction,cpt=0;
    unsigned int i=0,j=0;
    std::fstream level;

    // Opening the file in read mode
    level.open("./Levels/" + nameLvl + ".txt", std::ios::in);
    if (level.fail()) {
        std::cerr << "Can not open the " << nameLvl << ".txt  file. (need to be on ./Levels/)" << std::endl;
        exit(EXIT_FAILURE);
    }

    replace(nameLvl,"Editor/","");
    m_nameLvl=nameLvl;

    // Size of the grid
    level >> tmp;
    m_grid.setLines(tmp);
    level >> tmp;
    m_grid.setCols(tmp);

    m_grid.initGrid();

    // Adjusting the radius
    m_grid.AdjustingRadius();
    m_robot.updatePosition();

    // Position of the Robot
    level >> tmp;
    level >> tmp2;

    if((unsigned)tmp<=MAX_LINES && (unsigned)tmp2<=MAX_COLS
            && tmp>=0 && tmp2>=0){
        m_robot.setPosition(Position(tmp,tmp2));
    }else{
        std::cerr << "Position of robot failed" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Direction of the robot
    level >> direction;
    m_robot.setDirection(direction);

    // Max size Main, proc1 and proc2
    level >> m_maxMain;
    level >> m_maxProc1;
    level >> m_maxProc2;

    if(m_maxMain>MAX_ACTIONS_MAIN || m_maxProc1>MAX_ACTIONS_PROCS || m_maxProc2>MAX_ACTIONS_PROCS){
        std::cerr << "Maximum of Main or proc1 or proc2 failed" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Size Max
    if(m_grid.lines()<=MAX_LINES && m_grid.cols()<=MAX_COLS){

        // Initialization of the vector
        m_grid.initGrid();

        // Vector Fill
        while(i<m_grid.lines() && !level.eof()){
            j=0;
            while(j<m_grid.cols() && !level.eof()){
                level >> tmp;
                m_grid.set({(signed)i,(signed)j},tmp);
                j++;
                if(!level.eof())
                    cpt++;
            }
            i++;
        }
        // allow to know if we are at the end of the file
        level >> tmp;
        // If we are not at the end of the file or if the file is too small
        if((unsigned)cpt != m_grid.lines()*m_grid.cols() || !level.eof()){
            std::cerr << nameLvl << ".txt : corrupted file (false number of data)." << std::endl;
            exit(EXIT_FAILURE);
        }
    }else{
        // if the number of rows or columns is too large
        std::cerr << nameLvl << ".txt : corrupted file (overflow max grid size). "<< std::endl
                  << "max number of cols: "<< MAX_COLS << ", of lines: " << MAX_LINES << std::endl;
        exit(EXIT_FAILURE);
    }
}

/* used to load a level from the editor */
void Level::loadLvl(Grid* grid, Robot* robot,unsigned int maxMain,unsigned int maxProc1,unsigned int maxProc2)
{
    m_maxMain = maxMain;
    m_maxProc1 = maxProc1;
    m_maxProc2 = maxProc2;
    m_grid = *grid;
    m_grid.setGridXY({GRIDX_GAME,GRIDY_GAME});
    m_grid.setWidth(GRID_LVL_WIDTH);
    m_grid.setHeight(GRID_LVL_HEIGHT);
    m_grid.AdjustingRadius();
    m_robot= *robot;
    m_robot.setGrid(&m_grid);
    m_robot.setPosition(m_robot.getPosition());
}

/* used to load a level when he was confirm */
void Level::storeLevel(std::string const &name)
{
    std::fstream flux;

    // Opening the file in read mode
    flux.open("./Levels/Editor/"+name+".txt", std::ios::out);
    if (flux.fail()) {
        std::cerr << "Can open ./Levels/Editor/" << name << "on write mode"<< std::endl;
        exit(EXIT_FAILURE);
    }

    // Size of the grid
    flux << m_grid.lines() << std::endl;
    flux << m_grid.cols() << std::endl;

    // Position of the Robot
    flux << m_robot.getPosition().getLine() << std::endl;
    flux << m_robot.getPosition().getCol() << std::endl;

    // Direction of the robot
    flux << m_robot.getDirection() << std::endl;

    // Max size Main, proc1 and proc2
    flux << m_maxMain << std::endl;
    flux << m_maxProc1 << std::endl;
    flux << m_maxProc2 << std::endl;

    // Vector Fill
    for(int i=0;(unsigned)i<m_grid.lines();i++)
        for(int j=0;(unsigned)j<m_grid.cols();j++)
            flux << m_grid.get({i,j}) << std::endl;
}

/* Filled the program of the robot with all the action */
void Level::initProgram(int i)
{
    std::string action;
    if(i==0){
        for(Button* b : m_main){
            action = b->getAction();
            if(action=="1"){
                initProgram(1);
            }else if(action=="2"){
                initProgram(2);
            }else{
                m_robot.chargerProgram(action[0]);
            }
        }
    }else if(i==1){
        for(Button* b1 : m_proc1){
            action = b1->getAction();
            if(action=="1"){
                initProgram(1);
            }else if(action=="2"){
                initProgram(2);
            }else{
                m_robot.chargerProgram(action[0]);
            }
        }
    }else{
        for(Button* b2 : m_proc2){
            action = b2->getAction();
            if(action=="1"){
                initProgram(1);
            }else if(action=="2"){
                initProgram(2);
            }else{
                m_robot.chargerProgram(action[0]);
            }
        }
    }
}

/* clear the 3 vectors of actions */
void Level::clearInstructions()
{
    for(Button* b : m_main)
        delete b;
    m_main.clear();
    for(Button* b : m_proc1)
        delete b;
    m_proc1.clear();
    for(Button* b : m_proc2)
        delete b;
    m_proc2.clear();
}

/* run a step from the robot */
void Level::runProgram()
{
    m_robot.runStep();
}

bool Level::animationEnd() const
{
    return m_robot.animationEnd();
}

bool Level::programEnd() const
{
    return m_robot.end();
}

bool Level::win()const
{
    return m_grid.win();
}

/* draw te Level according to the "conditions" */
void Level::drawLvl(sf::RenderWindow &window, bool inConfirm, bool inFinal, bool Rect, sf::Vector2f const &m)
{
    // Display of the background
    window.draw(m_backgroundSprite);

    // Display of the grid
    m_grid.drawGrid(window);

    // Dipslay of the name
    if(!inConfirm)
        m_text.setString(m_nameLvl);
    else
        m_text.setString("Confirmer le niveau pour pouvoir le nommer !");
    m_text.setScale({0.8,0.8});
    m_text.setColor(sf::Color::Black);
    m_text.setPosition({860-m_text.getGlobalBounds().width/2,20});
    window.draw(m_text);
    m_text.setScale({0.6,0.6});
    m_text.setColor(sf::Color(138,160,225));

    // Display of max size
    m_text.setString(std::to_string(m_maxMain)+" instructions maximum");
    m_text.setPosition({230,270});
    window.draw(m_text);
    m_text.setString(std::to_string(m_maxProc1)+" instructions maximum");
    m_text.setPosition({230,475});
    window.draw(m_text);
    m_text.setString(std::to_string(m_maxProc2)+" instructions maximum");
    m_text.setPosition({230,675});
    window.draw(m_text);

    m_robot.drawRobot(window);

    // Display of the end of the level
    if(m_grid.win() && inFinal){
        // in standard level ...
        window.draw(m_endLvl);
        if(inConfirm){  // in new Level to confirm
            m_text.setString("Veuillez choisir un nom pour votre niveau :");
            m_text.setPosition({470,WINDOW_Y/2-70});
            window.draw(m_text);
        }
    }

    // Display of buttons
    for(Button* b : m_buttons){
        if(b->getAction()!="next" && b->getAction()!="previous")
            b->drawButton(window);
        else if(m_grid.win() && !inConfirm && inFinal){    // Draw the buttons just if the grid is win
            b->drawButton(window);
        }
    }
    for(Button* b : m_orders){
        b->drawButton(window);
    }
    for(Button* b : m_main){
        b->drawButton(window);
    }
    for(Button* b : m_proc1){
        b->drawButton(window);
    }
    for(Button* b : m_proc2){
        b->drawButton(window);
    }
    if(Rect) drawRect(m,window);

    if(!m_grid.win() && inFinal)
        window.draw(m_circle);
}

/* delete the Button* who are in vectors */
Level::~Level()
{
    for(Button* b : m_orders){
        delete b;
    }
    for(Button* b : m_main){
        delete b;
    }
    for(Button* b : m_proc1){
        delete b;
    }
    for(Button* b : m_proc2){
        delete b;
    }
}
