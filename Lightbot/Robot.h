#ifndef ROBOT_H
#define ROBOT_H

#include "Grid.h"

class Robot
{
private:
    // Main variables
    unsigned int m_nextInstruction;
    Position m_pos;
    int m_direction;
    std::string m_programme;
    Grid* m_grid;

    // Sprite and texture
    sf::Texture m_texture;
    sf::Sprite m_sprite;

    // Used to moving forward
    sf::Vector2f m_coeff;

    // Bools
    bool m_animationEnd;
    bool m_programEnd;

public:
    // Constructor
    Robot(Grid* grid, Position pos,std::string fileTexture);

    // Setters
    void setPosition(Position const &pos);
    void setPosition(sf::Vector2f const &pos);
    void setGrid(Grid* grid);
    void setDirection(unsigned int i);

    // Getter
    unsigned int getDirection();
    Position getPosition()const;
    bool animationEnd()const;
    bool end() const;
    unsigned int getProgSize()const;

    // Animations
    void animationForward();
    void animationLeftRotation();
    void animationRightRotation();
    void Rotation();

    // Program
    void chargerProgram(char action);
    void clearProgram();
    void initRotation();
    void runStep();

    // Others
    void updatePosition();
    bool contains(sf::Vector2f pos)const;

    // Draw
    void drawRobot(sf::RenderWindow &window);

    // Destroyer
    ~Robot() = default;
};

#endif // ROBOT_H
