//Copyright 2017 Sylvain Dupuy, Julien Bascouzaraix
/*
This file is part of Lightbot.

Lightbot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lightbot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Lightbot. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Menu.h"

Menu::Menu(std::string background)
    :m_buttons{}
{
    if (!m_background.loadFromFile("./Images/"+background))
    {
        std::cerr << "cant open the image." <<std::endl;
    }
    m_backgroundSprite.setTexture(m_background);
    m_backgroundSprite.setScale(WINDOW_X/m_backgroundSprite.getLocalBounds().width,WINDOW_Y/m_backgroundSprite.getLocalBounds().height);
}

void Menu::addButton(sf::Vector2f const &pos, std::string const &action,std::string const &nameTexture, bool canPressed, bool drawActions)
{
    Button* b = new Button{pos,action,nameTexture,canPressed,drawActions};
    m_buttons.push_back(b);
}

void Menu::addButton(sf::Vector2f const &pos,std::string const &action,std::string const &nameTexture,sf::Vector2f const &size,bool canPressed,bool drawActions)
{
    m_buttons.push_back(new Button{pos,action,nameTexture,size,canPressed,drawActions});
}

void Menu::drawMenu(sf::RenderWindow &window)
{
    window.draw(m_backgroundSprite);
    for(Button* b : m_buttons)
        b->drawButton(window);

}

/* return the first Button* who have the same action that the given action or nullptr */
Button* Menu::getButton(std::string action)
{
    auto same_action = [& action] (const Button * ptr) {
        return ptr->getAction()==action;
    };
    auto it = std::find_if(m_buttons.begin(), m_buttons.end(), same_action);

    return it == m_buttons.end() ? nullptr : *it;
}

/* Deleting all the buttons */
Menu::~Menu()
{
    for(Button* b : m_buttons)
        delete b;
}
