//Copyright 2017 Sylvain Dupuy, Julien Bascouzaraix
/*
This file is part of Lightbot.

Lightbot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lightbot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Lightbot. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Button.h"


void Button::update()
{
    sf::Vector2f pos;
    pos.x = m_sprite.getPosition().x - (m_textAction.getGlobalBounds().width) / 2;
    pos.y = m_sprite.getPosition().y - (m_textAction.getGlobalBounds().height) / 2 + m_textAction.getPosition().y - m_textAction.getGlobalBounds().top;
    m_textAction.setPosition(pos);
}

Button::Button(sf::Vector2f const & pos, std::string const & action, std::string nameTexture, bool canPressed,bool drawActions)
    :m_action{action}
    ,m_isPressed{false}
    ,m_drawAction{drawActions}
{
    if (!m_texture.loadFromFile("./Images/" + nameTexture))
    {
        std::cout << "cant open the image " << nameTexture << "." << std::endl;
        exit(EXIT_FAILURE);
    }
    if(canPressed){
        if (!m_texturePressed.loadFromFile("./Images/" + nameTexture.insert(nameTexture.find('.'),"Pushed")))
        {
            std::cout << "cant open the image " << nameTexture.insert(nameTexture.find('.'),"Pushed") << "." << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    m_texture.setSmooth(true);
    m_sprite.setTexture(m_texture);
    m_sprite.setOrigin(sf::Vector2f(m_sprite.getLocalBounds().width/2,m_sprite.getLocalBounds().height/2));
    m_sprite.setPosition(pos);
    if (!m_font.loadFromFile("font.ttf"))
    {
        std::cerr << "Please put the font.ttf file in the compilation directory" << std::endl;
    }
    m_textAction.setFont(m_font);
    m_textAction.setScale({0.8,0.8});
    m_textAction.setString(m_action);
    while(m_textAction.getGlobalBounds().width > m_sprite.getGlobalBounds().width-8)
        m_textAction.setScale(m_textAction.getScale()-sf::Vector2f{0.1,0.1});
    m_textAction.setColor(sf::Color::Black);
    update();
}

Button::Button(sf::Vector2f const & pos, std::string const & action, std::string nameTexture, sf::Vector2f const & size, bool canPressed, bool drawActions)
    :m_action{action}
    ,m_size{size}
    ,m_isPressed{false}
    ,m_drawAction{drawActions}
{
    if (!m_texture.loadFromFile("./Images/" + nameTexture))
    {
        std::cout << "cant open the image " << nameTexture << "." << std::endl;
        exit(EXIT_FAILURE);
    }
    if(canPressed){
        if (!m_texturePressed.loadFromFile("./Images/" + nameTexture.insert(nameTexture.find('.'),"Pushed") ))
        {
            std::cout << "cant open the image " << nameTexture.insert(nameTexture.find('.'),"Pushed") << "." << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    m_texture.setSmooth(true);
    m_sprite.setTexture(m_texture);
    m_sprite.setScale(size.x/m_sprite.getLocalBounds().width,size.y/m_sprite.getLocalBounds().height);
    m_sprite.setOrigin(sf::Vector2f(m_sprite.getLocalBounds().width/2,m_sprite.getLocalBounds().height/2));
    m_sprite.setPosition(pos);
    if (!m_font.loadFromFile("font.ttf"))
    {
        std::cerr << "Please put the font.ttf file in the compilation directory" << std::endl;
    }
    m_textAction.setFont(m_font);
    m_textAction.setString(m_action);
    m_textAction.setScale({0.8,0.8});
    while(m_textAction.getGlobalBounds().width > m_sprite.getGlobalBounds().width-8)
        m_textAction.setScale(m_textAction.getScale()-sf::Vector2f{0.1,0.1});
    m_textAction.setColor(sf::Color::Black);
    update();
}

bool Button::contains(sf::Vector2f const &pos) const
{
    return (m_sprite.getGlobalBounds().contains(pos));
}

bool Button::circleContains(sf::Vector2f const &pos) const
{
    return square_distance(pos, m_sprite.getPosition()) <= pow(m_size.x/2,2);
}

void Button::setPosition(sf::Vector2f const &pos)
{
    m_sprite.setPosition(pos);
}

sf::Vector2f Button::getPosition() const
{
    return m_sprite.getPosition();
}

sf::Vector2f Button::getSize() const
{
    return m_size;
}

void Button::drawButton(sf::RenderWindow & window)
{
    window.draw(m_sprite);
    if(m_drawAction)
        window.draw(m_textAction);
}

std::string Button::getAction() const
{
    return m_action;
}

/* its just use for the main menu button */
void Button::isPressed(bool b)
{
    if(b){
        m_sprite.setTexture(m_texturePressed);
        m_isPressed=true;
    }else{
        m_sprite.setTexture(m_texture);
        m_isPressed=false;
    }
}

bool Button::isPressed() const
{
    return m_isPressed;
}
