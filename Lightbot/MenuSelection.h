#ifndef MENUSELECTION_H
#define MENUSELECTION_H
#include "Menu.h"

class MenuSelection : public Menu // The class inherits of 'Menu'
{
private:
    unsigned int m_currentPage; // Number of the current page
    unsigned int m_choicePerPage; // Number of choice per page

    // Vector who contains the button that allow to choice a level
    std::vector<Button*> m_choiceButtons;
public:
    //Constructor
    MenuSelection(std::string rep,unsigned int i);

    // Getters
    unsigned int getNbChoiceButtons()const;
    unsigned int getNbPages()const;
    unsigned int getChoicePerPage()const;
    unsigned int getNbNext()const;
    unsigned int getCurrentPage()const;
    Button* getChoiceButton(sf::Vector2f m);

    //Setter
    void setCurrentPage(unsigned int p);

    // Add a button in the vector of choice buttons
    void addButtonChoice(sf::Vector2f const &pos, std::string const &action,std::string const &nameTexture,sf::Vector2f const &size, bool canPressed = false, bool drawActions=false);

    //
    bool erase(sf::Vector2f m, std::string &action);

    // Draw
    void drawSelectionMenu(sf::RenderWindow & window);

    // The destroyer
    ~MenuSelection();
};

#endif // MENUSELECTION_H
