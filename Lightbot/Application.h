#ifndef APPLICATION_H
#define APPLICATION_H

#include "Level.h"
#include "Editor.h"
#include "MenuSelection.h"

class Application

{
private:
    /***************************************** Variables *****************************************/

    // Declaration of Enums
    enum class Place {MAIN_MENU , SELECTION_LVL ,SELECTION_EDITOR ,EDITOR ,GAME,GAME_EDITOR,CONFIRM,END_MENU} m_place;  // Where is the player ?
    enum class State {INITIAL ,ADDING ,MOUVEMENT, ROBOT} m_state;      // Are we moving actions ? (or moving robot in editor)
    enum class Program {INITIAL ,STEP ,RUN ,PAUSE, FINAL} m_program;   // What is the status of the program ?

    bool m_running, m_button_pressed;
    int                m_button;
    sf::RenderWindow   m_window;
    sf::Vector2f       m_start;              // Used for the offset
    sf::Vector2f       m_offset;             //        ``
    sf::Vector2f       m_mouse;              // Mouse position
    unsigned int       m_currentLvl;         // Current level number (for official level)
    std::vector<std::string> m_namesLvl;     // Contains the names of the officials levels
    unsigned int       m_currentLvlEditor;   // Current level number (for editor level)
    bool               m_confirmName;        // Are we confirm the name in the editor ?

    // The Level and the differents menus
    Level              m_lvl;
    Menu               m_mainMenu;
    MenuSelection      m_selectionLvlMenu;
    MenuSelection      m_selectionEditorMenu;
    Menu               m_menuEnd;

    // The editor
    Editor             m_editor;
    std::vector<std::string> m_namesLvlEditor; // Contains the names of the levels created in the editor

    // Text Variables
    sf::Font           m_font;
    sf::Text           m_textName;           // Used in the editor
    std::string        m_nameLvl;
    std::string        m_allowed_char;       // Table of allowed char

    // The differents Button used to moving Actions
    Button             m_buttonTmp;          // use if the player adding an action
    Button*            m_buttonPressed;      // use if the player moving an action

    /***************************************** Functions *****************************************/

    // internal functions (Events)
    void mousePressed();
    void mouseReleased();
    void mouseMoved();
    void mouseDragged();
    void key_pressed(sf::Event::KeyEvent event);
    void text_entered(sf::Event event);

    // others functions
    void stop();
    void update();
    void initLvl();
    void initName();
    bool inGame()const;
    void draw();
    void process_events();
    void level();

public:
    Application();
    ~Application();
    void run();
};

#endif // APPLICATION_H
