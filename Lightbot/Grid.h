#ifndef GRID_H
#define GRID_H

#include "Position.h"

class Grid
{
private:
    // Main variables
    sf::CircleShape m_hex;
    unsigned int m_nblines=0;
    unsigned int m_nbcols=0;
    std::vector<int> m_grid;     // Contains 'int' which refers to the type of case
    static float m_margin;       // Margin between size and the grid
    float m_radius;              // Radius of the hexagones

    // Stack for save the lines and cols delete (editor)
    std::stack<std::string> m_stackCols;
    std::stack<std::string> m_stackLines;

    // Positions
    sf::Vector2f m_gridXY={0,0}; // Starting Position of size
    sf::Vector2f m_size;         // Width and Height max where grid can be
    sf::Vector2f m_firstHex;     // Position of the center of the first hexagon
public:
    // Constructors
    Grid();
    Grid(unsigned int lines, unsigned int cols, sf::Vector2f gridXY, sf::Vector2f size);
    // Getters
    unsigned int lines()const;
    unsigned int cols()const;
    float firstX()const;
    int get(Position const &pos)const;
    float getRadius()const;
    float firstY()const;
    // Setters
    void set(Position const &pos, int contenu);
    void setLines(unsigned int nblines);
    void setCols(unsigned int nbcols);
    void setGridXY(sf::Vector2f const & pos);
    void setWidth(float w);
    void setHeight(float h);
    // Others
    void AdjustingRadius();
    void addCol();
    void removeCol();
    void addLine();
    void removeLine();
    void initGrid();
    bool contains(Position const &pos) const;
    void drawGrid(sf::RenderWindow &window);
    bool win()const;
};

#endif // GRID_H
