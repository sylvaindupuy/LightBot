//Copyright 2017 Sylvain Dupuy, Julien Bascouzaraix
/*
This file is part of Lightbot.

Lightbot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lightbot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Lightbot. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Editor.h"

Editor::Editor(std::string background)
    :Menu(background)
    ,m_grid{MAX_LINES,MAX_COLS,{GRIDX_EDITOR,GRIDY_EDITOR},{GRID_EDITOR_WIDTH,GRID_EDITOR_HEIGHT}}
    ,m_robot{&m_grid,Position(0,0),"Tools/Robot.png"}
{
    if(!m_font.loadFromFile("font.ttf"))
        exit(EXIT_FAILURE);
    m_textMax.setFont(m_font);
    m_textMax.setScale({0.8,0.8});
    m_textMax.setColor(sf::Color(138,160,225));

    m_grid.initGrid();
    m_grid.AdjustingRadius();
    m_robot.setPosition(m_robot.getPosition());
    m_robot.updatePosition();
    addButton({1100,500},"Confirmation","Tools/confirmation.png",{227,81});
    addButton({1050,100},"lineMinus","Tools/minus.png",{40,40});
    addButton({1000,100},"linePlus","Tools/plus.png",{40,40});
    addButton({1150,100},"colMinus","Tools/minus.png",{40,40});
    addButton({1200,100},"colPlus","Tools/plus.png",{40,40});
    addButton({1100,600},"mainMenu","Tools/mainMenuEditor.png",{227,81});

    addButton({1000,315},"mainMinus","Tools/minus.png",{30,30});
    addButton({1000,365},"proc1Minus","Tools/minus.png",{30,30});
    addButton({1000,415},"proc2Minus","Tools/minus.png",{30,30});
    addButton({1180,315},"mainPlus","Tools/plus.png",{30,30});
    addButton({1180,365},"proc1Plus","Tools/plus.png",{30,30});
    addButton({1180,415},"proc2Plus","Tools/plus.png",{30,30});
}

Robot* Editor::getRobot(){
    return &m_robot;
}

void Editor::changeMaxs(unsigned int i)
{
    switch (i) {
    case 0:
        if(m_maxMain > 0)
            m_maxMain--;
        break;
    case 1:
        if(m_maxMain < MAX_ACTIONS_MAIN)
            m_maxMain++;
        break;
    case 2:
        if(m_maxProc1 > 0)
            m_maxProc1--;
        break;
    case 3:
        if(m_maxProc1 < MAX_ACTIONS_PROCS)
            m_maxProc1++;
        break;
    case 4:
        if(m_maxProc2 > 0)
            m_maxProc2--;
        break;
    case 5:
        if(m_maxProc2 < MAX_ACTIONS_PROCS)
            m_maxProc2++;
        break;
    default:
        break;
    }
}

unsigned int Editor::getMaxMain() const
{
    return m_maxMain;
}

unsigned int Editor::getMaxProc1() const
{
    return m_maxProc1;
}

unsigned int Editor::getMaxProc2() const
{
    return m_maxProc2;
}

void Editor::changeLines(int i)
{
    switch (i) {
    case 1:
        if(m_grid.lines()<MAX_LINES)
            m_grid.addLine();
        break;
    case -1:
        if(m_grid.lines()>1)
            m_grid.removeLine();
        break;
    default:
        break;
    }
    m_grid.AdjustingRadius();
    m_robot.updatePosition();
}

void Editor::changeCols(int i)
{
    switch (i) {
    case 1:
        if(m_grid.cols()<MAX_COLS)
            m_grid.addCol();
        break;
    case -1:
        if(m_grid.cols()>1){
            m_grid.removeCol();
        }
        break;
    default:
        break;
    }
    m_grid.AdjustingRadius();
    m_robot.updatePosition();
}

void Editor::clearEditor()
{
    m_grid.setLines(MAX_LINES);
    m_grid.setCols(MAX_COLS);
    m_grid.initGrid();
    m_robot.setPosition(Position{0,0});
    m_robot.setDirection(0);
    m_grid.AdjustingRadius();
    m_robot.updatePosition();
}

Grid *Editor::getGrid()
{
    return &m_grid;
}

bool Editor::contains(Position const &pos) const
{
    return m_grid.contains(pos) && m_grid.get(pos)!=0;
}

/* We can confirm only if we have at least 1 case to ligth */
bool Editor::canConfirm()
{
    bool can = false;
    for(int j=0;(unsigned)j<m_grid.cols();j++){
        for(int i=0;(unsigned)i<m_grid.lines();i++){
            if(m_grid.get({i,j})==2)
                can = true;
        }
    }
    return can;
}

/* This function converted mouse position to Position on the grid */
/* Thanks to redblobgames for his help !
   the website : http://www.redblobgames.com/grids/hexagons/      */
Position Editor::mouseToHex(sf::Vector2f const &m) const
{
    float radius = m_grid.getRadius();
    float X = m.x - (m_grid.firstX()- radius) - radius;

    float Y = m.y - (m_grid.firstY()- radius) - radius;

    // Conversion to cubic coordinates
    float x = X * (2. / 3.) / radius;

    float z = ((-X / 3.) + sqrt(3.)/3. * Y ) / radius;

    float y = - ( x + z );

    // Rounded cubic coordinates
    int rx = std::round(x);
    int ry = std::round(y);
    int rz = std::round(z);

    // Deviations from roundings
    float xd = std::abs(rx - x);
    float yd = std::abs(ry - y);
    float zd = std::abs(rz - z);

    if ( xd > yd  && xd > zd )
        rx = - ( ry + rz );       // Xd is rectified if it has the largest deviation
    else if ( yd > zd )
        ry = - ( rx + rz);        // Or zd if greater than yd
    else
        rz = - ( rx + ry );

    //axial to offset
    rz = rz + (rx + (rx & 1)) / 2;

    return {rz,rx};
}


void Editor::changeCase(Position const &p)
{
    if(m_grid.contains(p)){
        int nb = m_grid.get(p);
        if(m_robot.getPosition()!=p){
            if(nb==2)
                m_grid.set(p,0);
            else
                m_grid.set(p,nb+1);
        }else if(m_robot.getPosition()==p){
            if(nb!=2)
                m_grid.set(p,2);
            else
                m_grid.set(p,1);
        }
    }
}

void Editor::drawEditor(sf::RenderWindow &window)
{
    this->drawMenu(window);
    m_grid.drawGrid(window);
    m_robot.drawRobot(window);

    m_textMax.setString("Max Main: "+std::to_string(m_maxMain));
    m_textMax.setPosition({1025,300});
    window.draw(m_textMax);

    m_textMax.setString("Max Proc1: "+std::to_string(m_maxProc1));
    m_textMax.setPosition({1025,350});
    window.draw(m_textMax);

    m_textMax.setString("Max Proc2: "+std::to_string(m_maxProc2));
    m_textMax.setPosition({1025,400});
    window.draw(m_textMax);
}
