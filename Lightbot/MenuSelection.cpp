#include "MenuSelection.h"
#include <stdio.h>

MenuSelection::MenuSelection(std::string rep, unsigned int i)
    :Menu(rep)
    ,m_currentPage{1}
    ,m_choicePerPage{i}
{}

void MenuSelection::addButtonChoice(const sf::Vector2f &pos, const std::string &action, const std::string &nameTexture, const sf::Vector2f &size, bool canPressed, bool drawActions)
{
    // If its the first page, we need to add the Arrow buttons
    if(m_choiceButtons.size()==m_choicePerPage){
        this->addButton(sf::Vector2f(850,625),"nextPage","Tools/nextPage.png",{50,50});
        this->addButton(sf::Vector2f(350,625),"previousPage","Tools/previousPage.png",{50,50});
    }
    m_choiceButtons.push_back(new Button{pos,action,nameTexture,size,canPressed,drawActions});
}

bool MenuSelection::erase(sf::Vector2f m, std::string & action)
{
    unsigned int j=0;
    Button* b = getChoiceButton(m);
    bool erase = b;
    if(b){
        action = b->getAction();
        auto it = std::find(m_choiceButtons.begin(),m_choiceButtons.end(),b);
        m_choiceButtons.erase(it);
        const char* n = std::string("Levels/Editor/" + action + ".txt").c_str();
        std::remove(n);
        delete b;
    }
    for(unsigned int i=0;i<m_choiceButtons.size();i++){
        m_choiceButtons[i]->setPosition(positionsLevelChoice(j));
        if(j==m_choicePerPage-1) j=0;
        else
            j++;
    }
    if(m_choiceButtons.size() == m_choicePerPage){
        auto b = m_buttons[m_buttons.size()-1];
        m_buttons.pop_back();
        delete b;
        b = m_buttons[m_buttons.size()-1];
        m_buttons.pop_back();
        delete b;
        m_currentPage=1;
    }else if (m_choiceButtons.size()%m_choicePerPage == 0 && m_currentPage==getNbPages()+1 )
        m_currentPage--;
    for(Button* b : m_choiceButtons)
        b->update();
    return erase;
}

void MenuSelection::drawSelectionMenu(sf::RenderWindow &window)
{
    unsigned int begin = (m_currentPage-1)*m_choicePerPage;
    unsigned int nbLvlToDraw = m_choicePerPage;
    if(begin+m_choicePerPage > m_choiceButtons.size())
        nbLvlToDraw = m_choiceButtons.size() - begin;

    // Draw the "tools" buttons
    if(m_currentPage==1){
        window.draw(m_backgroundSprite);
        for(Button* b : m_buttons){
            if(!(b->getAction()=="previousPage"))
                b->drawButton(window);
        }
    }else if(m_currentPage==getNbPages()){
        window.draw(m_backgroundSprite);
        for(Button* b : m_buttons){
            if(!(b->getAction()=="nextPage"))
                b->drawButton(window);
        }
    }else
        this->drawMenu(window);

    // Draw the choice buttons according to the page
    for(unsigned int i=begin;i<begin+nbLvlToDraw;i++)
        m_choiceButtons[i]->drawButton(window);
}

unsigned int MenuSelection::getNbChoiceButtons() const
{
    return m_choiceButtons.size();
}

unsigned int MenuSelection::getNbPages() const
{
    return m_choiceButtons.size()%m_choicePerPage == 0 ? m_choiceButtons.size()/(m_choicePerPage) : m_choiceButtons.size()/(m_choicePerPage)+1;
}

unsigned int MenuSelection::getChoicePerPage() const
{
    return m_choicePerPage;
}

/* we are looking for just for the button according to the page */
Button *MenuSelection::getChoiceButton(sf::Vector2f m)
{
    bool trouve=false;
    unsigned int startingPosition = (m_currentPage-1)*m_choicePerPage;
    unsigned int i = startingPosition;
    unsigned int nbButtonsInPage = (m_choiceButtons.size() - (m_currentPage-1)*m_choicePerPage);
    while(i < startingPosition+nbButtonsInPage && !trouve){
        if(m_choiceButtons[i]->contains(m))
            trouve=true;
        else
            i++;
    }
    return trouve == false ? nullptr : m_choiceButtons[i];
}

unsigned int MenuSelection::getNbNext() const
{
    return m_choiceButtons.size()%m_choicePerPage;
}

unsigned int MenuSelection::getCurrentPage() const
{
    return m_currentPage;
}

void MenuSelection::setCurrentPage(unsigned int p)
{
    m_currentPage = p;
}

/* Deleting all the buttons */
MenuSelection::~MenuSelection()
{
    for(auto b : m_choiceButtons)
        delete b;
}
