#ifndef POSITION_H
#define POSITION_H

#include "tools.h"

class Grid;

class Position
{
private:
    int m_line=0;
    int m_col=0;
public:
    // Constructors
    Position() = default;
    Position(int line,int col);

    // Getters
    int getLine()const;
    int getCol()const;
    Position next(int dir)const;

    // Converting position to pixels
    sf::Vector2f PositionToPixels(const Grid * const g)const;

    // Operator overloads
    bool operator !=(Position const &p)const;
    bool operator ==(Position const &p)const;
};

#endif // POSITION_H
