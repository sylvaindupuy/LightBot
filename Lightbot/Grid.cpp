//Copyright 2017 Sylvain Dupuy, Julien Bascouzaraix
/*
This file is part of Lightbot.

Lightbot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lightbot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Lightbot. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Grid.h"

float Grid::m_margin = 40.f;

Grid::Grid()
    :m_hex{m_radius,6}
{
    m_hex.setOrigin(sf::Vector2f(m_radius,m_radius));
    m_hex.setRotation(90.f);
    m_hex.setOutlineColor(sf::Color::Black);
    m_hex.setOutlineThickness(1.f);
}

Grid::Grid(unsigned int lines, unsigned int cols,sf::Vector2f gridXY, sf::Vector2f size)
    :m_hex{m_radius,6}
    ,m_nblines{lines}
    ,m_nbcols{cols}
    ,m_gridXY{gridXY}
    ,m_size{size}
{
    m_hex.setOrigin(sf::Vector2f(m_radius,m_radius));
    m_hex.setRotation(90.f);
    m_hex.setOutlineColor(sf::Color::Black);
    m_hex.setOutlineThickness(1.f);
}

unsigned int Grid::lines() const
{
    return m_nblines;
}

unsigned int Grid::cols() const
{
    return m_nbcols;
}

float Grid::firstX() const
{
    return m_firstHex.x;
}

float Grid::firstY() const
{
    return m_firstHex.y;
}

void Grid::setGridXY(sf::Vector2f const &pos)
{
    m_gridXY = pos;
}

void Grid::setWidth(float w)
{
    m_size.x = w;
}

void Grid::setHeight(float h)
{
    m_size.y =h;
}

float Grid::getRadius() const
{
    return m_radius;
}

/* This function Adjusting the radius according to the number of lines and cols (and update the first position too) */
void Grid::AdjustingRadius()
{
    // Calculing for Height
    float potentialHeight;
    float radius = (m_size.y-m_margin*2)/(m_nblines*2);
    if(m_nbcols == 1)
        potentialHeight = (radius*sin(M_PI/3)*m_nblines)*2;
    else
        potentialHeight = radius*sin(M_PI/3) + (radius*sin(M_PI/3)*m_nblines)*2;
    while((m_size.y-m_margin*2)-potentialHeight > 1)
    {
        radius += 0.2;
        if(m_nbcols == 1)
            potentialHeight = (radius*sin(M_PI/3)*m_nblines)*2;
        else
            potentialHeight = radius*sin(M_PI/3)+(radius*sin(M_PI/3)*m_nblines)*2;
    }

    // Calculing for Width
    float radius2 = (m_size.x-m_margin*2)/(m_nbcols*2);
    if(m_nbcols > 1){
        float potentialWidth = radius2*2 + (radius2+radius2*cos(M_PI/3))*(m_nbcols-1);
        while((m_size.x-m_margin*2)-potentialWidth > 1)
        {
            radius2 += 0.2;
            potentialWidth= radius2*2 + (radius2+radius2*cos(M_PI/3))*(m_nbcols-1);
        }
    }

    // We take the smallest
    m_radius = radius > radius2 ? radius2 : radius;

    m_hex.setRadius(m_radius);
    m_hex.setOrigin({m_radius,m_radius});

    // Updating the first position
    m_firstHex.x = m_gridXY.x + m_size.x/2 - ((m_nbcols-1)*(m_radius+(m_radius*cos(M_PI/3)))/2);
    m_firstHex.y = m_gridXY.y + m_size.y/2 - (m_nblines-1)*(m_radius*sin(M_PI/3));
    if(m_nbcols > 1)
        m_firstHex.y += m_radius*sin(M_PI/3)/2;
}

void Grid::addCol()
{

    std::string tmp = m_stackCols.top();
    // if the col who remove before is smaller
    if(m_nblines > tmp.size()){
        unsigned int a = m_nblines-tmp.size();
        for(unsigned int i=0;i<a;i++)
            tmp +="1";
    }

    unsigned int i=m_nbcols;
    for(unsigned int j=0;j<m_nblines;j++){
        m_grid.insert(m_grid.begin()+(i+j),std::stoi(std::string(1,tmp[j])));
        i += m_nbcols;
    }
    m_stackCols.pop();
    m_nbcols++;
}

/* To save what the player have done, we use a stack */
void Grid::removeCol()
{
    std::string tmp = "";
    unsigned int i=m_nbcols;
    for(unsigned int j=0;j<m_nblines;j++){
        tmp += std::to_string(m_grid[(i-1)-j]);
        m_grid.erase(m_grid.begin()+((i-1)-j));
        i += m_nbcols;
    }
    m_stackCols.push(tmp);
    m_nbcols--;
}

void Grid::addLine()
{

    std::string tmp = m_stackLines.top();
    // if the line who remove before is smaller
    if(m_nbcols > tmp.size()){
        unsigned int a = m_nbcols-tmp.size();
        for(unsigned int i=0;i<a;i++)
            tmp.insert(tmp.begin(),'1');
    }

    for(int i=m_nbcols-1;i>-1;i--){
        m_grid.push_back(std::stoi(std::string(1,tmp[i])));
    }
    m_stackLines.pop();
    m_nblines++;
}

void Grid::removeLine()
{
    std::string tmp="";
    for(unsigned int i=0;i<m_nbcols;i++){
        tmp += std::to_string(m_grid[m_grid.size()-1]);
        m_grid.erase(m_grid.end()-1);
    }
    m_stackLines.push(tmp);
    m_nblines--;
}

void Grid::setCols(unsigned int nbcols)
{
    m_nbcols = nbcols;
}

/* Fill the vector of grey case */
void Grid::initGrid()
{
    m_grid.clear();
    for(unsigned int i=0;i<m_nbcols*m_nblines;i++)
        m_grid.push_back(1);
}

void Grid::setLines(unsigned int nblines)
{
    m_nblines = nblines;
}

int Grid::get(Position const &pos) const
{
    return m_grid[pos.getLine()*m_nbcols+pos.getCol()];
}

void Grid::set(Position const &pos,int contenu)
{
    m_grid[pos.getLine()*m_nbcols+pos.getCol()] = contenu;
}

bool Grid::contains(Position const &pos) const
{
    return ((unsigned)pos.getLine() < m_nblines
            && (unsigned)pos.getCol() < m_nbcols
            && pos.getLine() >=0
            && pos.getCol() >=0);
}

void Grid::drawGrid(sf::RenderWindow &window)
{
    Position p;
    // For each positions
    for(int i=0;(unsigned)i<m_nblines;i++){
        for(int j=0;(unsigned)j<m_nbcols;j++){
            // set the position (in pixels)
            p = Position{i,j};
            m_hex.setPosition(p.PositionToPixels(this));
            // Choice of color according to vector
            switch(get(p))
            {
            case 1:
                m_hex.setFillColor(sf::Color(128,128,128));
                window.draw(m_hex);
                break;
            case 2:
                m_hex.setFillColor(sf::Color::Blue);
                window.draw(m_hex);
                break;
            case 3:
                m_hex.setFillColor(sf::Color::Yellow);
                window.draw(m_hex);
                break;
            }
        }
    }
}

/* Its win if we have no case to light */
bool Grid::win() const
{
    auto it = std::find(m_grid.begin(),m_grid.end(),2);
    return it == m_grid.end() ? true : false;
}
