#ifndef MENU_H
#define MENU_H

#include "Button.h"
#include "Robot.h"

class Menu
{
protected:
    // Sprites and texture
    sf::Sprite m_backgroundSprite;
    sf::Texture m_background;
    // Buttons
    std::vector<Button*> m_buttons;
public:
    // Constructor
    Menu(std::string background);

    // Differents way adding a button
    void addButton(sf::Vector2f const &pos, std::string const &action, std::string const &nameTexture,bool canPressed = false, bool drawActions=false);
    void addButton(sf::Vector2f const &pos, std::string const &action, std::string const &nameTexture, sf::Vector2f const &size, bool canPressed = false, bool drawActions=false);

    // Getter
    Button* getButton(std::string action);

    // Draw
    void drawMenu(sf::RenderWindow & window);
    ~Menu();
};

#endif // MENU_H
