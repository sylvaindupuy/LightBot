#ifndef BUTTON_H
#define BUTTON_H

#include "Position.h"

class Button
{

private:
    // Text variables
    std::string m_action;  // the "Name" of the button or the action (ex: "A" -> moving forward)
    sf::Font m_font;
    sf::Text m_textAction;

    // Sprite and textures
    sf::Texture m_texture;
    sf::Texture m_texturePressed; // is just used for buttons who can be pressed
    sf::Sprite m_sprite;
    sf::Vector2f m_size;

    // Others
    bool m_isPressed;      // For buttons who can be pressed, is he pressed ?
    bool m_drawAction;     // Do you need to draw the "action" ?
public:
    // the differents constructors
    Button() = default;
    Button(sf::Vector2f const & pos, std::string const & action, std::string nameTexture, bool canPressed=false, bool drawActions=false);
    Button(sf::Vector2f const & pos, std::string const & action, std::string nameTexture, sf::Vector2f const & size,bool canPressed=false,bool drawActions=false);
    // Getters
    sf::Vector2f getPosition()const;
    sf::Vector2f getSize()const;
    std::string getAction()const;
    bool isPressed()const;
    // Setters
    void isPressed(bool b);
    void setPosition(sf::Vector2f const &pos);
    void setSize(sf::Vector2f const & size);
    // Others
    void update();         // Set position for the text "action"
    bool contains(sf::Vector2f const &pos)const;
    bool circleContains(sf::Vector2f const &pos)const;
    void drawButton(sf::RenderWindow & window);
};

#endif // BUTTON_H
