//Copyright 2017 Sylvain Dupuy, Julien Bascouzaraix
/*
This file is part of Lightbot.

Lightbot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Lightbot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Lightbot. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Robot.h"

Robot::Robot(Grid *grid, Position pos, std::string fileTexture)
    :m_nextInstruction{0}
    ,m_pos{pos}
    ,m_direction{0}
    ,m_grid{grid}
    ,m_animationEnd{true}
    ,m_programEnd{false}
{
    if (!m_texture.loadFromFile("./Images/" + fileTexture))
    {
        std::cerr << "cant open the image " << fileTexture << "." << std::endl;
        exit(EXIT_FAILURE);
    }
    m_texture.setSmooth(true);

    m_sprite.setTexture(m_texture);
    m_sprite.setScale(sf::Vector2f(50/m_sprite.getLocalBounds().width,50/m_sprite.getLocalBounds().height));
    m_sprite.setOrigin(sf::Vector2f(m_sprite.getLocalBounds().width/2,m_sprite.getLocalBounds().height/2));
    m_sprite.setPosition(pos.PositionToPixels(m_grid));
}

void Robot::drawRobot(sf::RenderWindow &window)
{
    window.draw(m_sprite);
}

/* This function recalculates the position of the robot according to its position */
void Robot::updatePosition()
{
    if(m_grid->contains(m_pos))
        m_sprite.setPosition(m_pos.PositionToPixels(m_grid));
    else{
        m_pos = {0,0};
        m_grid->set({0,0},1);
        m_sprite.setPosition(m_pos.PositionToPixels(m_grid));
    }
    m_sprite.setScale(sf::Vector2f(m_grid->getRadius()/m_sprite.getLocalBounds().width,m_grid->getRadius()/m_sprite.getLocalBounds().height));
}

void Robot::setPosition(Position const &pos)
{
    m_pos=pos;
    m_sprite.setPosition(pos.PositionToPixels(m_grid));
}

void Robot::setPosition(sf::Vector2f const &pos){
    m_sprite.setPosition(pos);
}

Position Robot::getPosition() const
{
    return m_pos;
}

void Robot::setGrid(Grid *grid)
{
    m_grid = grid;
}

unsigned int Robot::getDirection()
{
    return m_direction;
}

void Robot::setDirection(unsigned int i)
{
    m_direction = i;
    m_sprite.setRotation(60*i);
}

void Robot::initRotation()
{
    m_sprite.setRotation(0);
    m_direction=0;
}

void Robot::chargerProgram(char action)
{
    m_programme+=action;
}

bool Robot::contains(sf::Vector2f pos) const
{
    return square_distance(pos, m_sprite.getPosition()) <= pow(m_sprite.getGlobalBounds().height/2,2);
}

void Robot::animationRightRotation()
{
    bool end=false;
    m_animationEnd=false;
    int rotation = m_sprite.getRotation();
    switch (m_direction) {
    case 5:
        if(rotation==0)
            end=true;
        break;
    case 0:
        if(rotation==60)
            end=true;
        break;
    case 1:
        if(rotation==120)
            end=true;
        break;
    case 2:
        if(rotation==180)
            end=true;
        break;
    case 3:
        if(rotation==240)
            end=true;
        break;
    case 4:
        if(rotation==300)
            end=true;
        break;
    default:
        break;
    }
    if(!end)
        m_sprite.setRotation(rotation+2);
    else{
        if(m_direction!=5)
            m_direction++;
        else
            m_direction=0;
        m_animationEnd=true;
    }
}

void Robot::Rotation()
{
    int rotation = m_sprite.getRotation();
    switch (m_direction) {
    case 5:
        m_sprite.setRotation(0);
        m_direction=0;
        break;
    case 0:
    case 2:
    case 3:
    case 1:
    case 4:
        m_sprite.setRotation(rotation+60);
        m_direction++;
        break;
    default:
        break;
    }
}

void Robot::animationLeftRotation()
{
    bool end=false;
    m_animationEnd=false;
    int rotation = m_sprite.getRotation();
    switch (m_direction) {
    case 5:
        if(rotation==240)
            end=true;
        break;
    case 0:
        if(rotation==300)
            end=true;
        break;
    case 2:
        if(rotation==60)
            end=true;
        break;
    case 3:
        if(rotation==120)
            end=true;
        break;
    case 1:
        if(rotation==0)
            end=true;
        break;
    case 4:
        if(rotation==180)
            end=true;
        break;
    default:
        break;
    }
    if(!end)
        m_sprite.setRotation(rotation-2);
    else{
        if(m_direction!=0)
            m_direction--;
        else
            m_direction=5;
        m_animationEnd=true;
    }
}

void Robot::animationForward()
{
    sf::Vector2f posFinal = m_pos.next(m_direction).PositionToPixels(m_grid);
    sf::Vector2f pos = m_sprite.getPosition();
    if(m_animationEnd){
        m_animationEnd=false;
        m_coeff.x = (posFinal.x-pos.x)/30.f;
        m_coeff.y = (posFinal.y-pos.y)/30.f;
    }
    switch (m_direction) {
    case 0: // North
        m_sprite.setPosition({pos.x,pos.y+m_coeff.y});
        break;
    case 1: // North East
        m_sprite.setPosition({pos.x+m_coeff.x,pos.y+m_coeff.y});
        break;
    case 2: // South East
        m_sprite.setPosition({pos.x+m_coeff.x,pos.y+m_coeff.y});
        break;
    case 3: // South
        m_sprite.setPosition({pos.x,pos.y+m_coeff.y});
        break;
    case 4: // South West
        m_sprite.setPosition({pos.x+m_coeff.x,pos.y+m_coeff.y});
        break;
    case 5: // North West
        m_sprite.setPosition({pos.x+m_coeff.x,pos.y+m_coeff.y});
        break;
    default:
        break;
    }
    if(abs(posFinal.x-m_sprite.getPosition().x) <1
            && abs(posFinal.y-m_sprite.getPosition().y) <1){
        m_animationEnd = true;
        m_sprite.setPosition(posFinal);
        m_pos=m_pos.next(m_direction);
    }
}

bool Robot::animationEnd() const
{
    return m_animationEnd;
}

void Robot::clearProgram()
{
    m_programme="";
    m_nextInstruction=0;
    m_animationEnd=true;
}


void Robot::runStep()
{
    switch (m_programme[m_nextInstruction]) {
    case 'A':
        if(m_grid->contains(m_pos.next(m_direction))
                && m_grid->get(m_pos.next(m_direction))!=0)
            animationForward();
        break;
    case 'D':
        animationRightRotation();
        break;
    case 'G':
        animationLeftRotation();
        break;
    case 'L':
        if(m_grid->get(m_pos)==2)
            m_grid->set(m_pos,3);
        break;
    default:
        break;
    }
    if(m_animationEnd)
        m_nextInstruction++;
    if(m_nextInstruction >= m_programme.size()){
        m_programEnd=true;
        clearProgram();
    }else
        m_programEnd=false;
}

bool Robot::end() const
{
    return m_programEnd;
}

unsigned int Robot::getProgSize() const
{
    return m_programme.size();
}
