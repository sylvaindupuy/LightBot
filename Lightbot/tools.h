#ifndef TOOLS_H
#define TOOLS_H

// All includes are here
#include <string>
#include <dirent.h> // To browse the directory
#include <cstring>  // for windows (strcmp)
#include <SFML/Graphics.hpp>
#include <math.h>   // For calculations
#include <vector>
#include <fstream>  // Input/output stream class to operate on files
#include <iostream> // for std::cerr, std::endl ...
#include <stack>    // Used in grid when we add / remove  cols / lines

const float ORDERS_WIDTH=40.f;
const float ORDERS_HEIGHT=40.f;
const float GRIDX_GAME=468.f;                      // Starting position of the frame
const float GRIDY_GAME=27.f;
const float GRIDX_EDITOR=25.f;                     // Starting position of the frame
const float GRIDY_EDITOR=29.f;
const unsigned int GRID_EDITOR_WIDTH=885;          // Size of the frame
const unsigned int GRID_EDITOR_HEIGHT=655;
const unsigned int GRID_LVL_WIDTH=785;             // Size of the frame
const unsigned int GRID_LVL_HEIGHT=582;
const unsigned int ACTIONS_BY_LINE=7;              // Number of actin by line
const unsigned int MAX_ACTIONS_MAIN=21;            // Number max of actions
const unsigned int MAX_ACTIONS_PROCS=14;
const unsigned int ORDERS_SPACING=15;              // this is hte spacing between orders
const unsigned int WINDOW_X=1280;                  // Size of the window
const unsigned int WINDOW_Y=720;
const unsigned int MAX_LINES=12;                   // Maximum number of lines
const unsigned int MAX_COLS=20;                    // Maximum number of cols
const unsigned int ORDERS_X=563;                   // Orders
const unsigned int ORDERS_Y=673;
const unsigned int MAIN_X=20;                      // MAIN
const unsigned int MAIN_Y=108;
const unsigned int MAIN_WIDTH=187;
const unsigned int MAIN_HEIGHT=403;
const unsigned int PROC1_X=20;                     // PROC1
const unsigned int PROC1_Y=343;
const unsigned int PROC1_WIDTH=154;
const unsigned int PROC1_HEIGHT=403;
const unsigned int PROC2_X=20;                     // PROC2
const unsigned int PROC2_Y=545;
const unsigned int PROC2_WIDTH=154;
const unsigned int PROC2_HEIGHT=403;

float square_distance(const sf::Vector2f &p1,
                     const sf::Vector2f &p2);

sf::Vector2f positionsActions(unsigned int i, unsigned int a);

sf::Vector2f positionsRectBetween(unsigned int i, unsigned int a);

sf::Vector2f positionsLevelChoice(unsigned int i);

void replace(std::string& str, const std::string& from, const std::string& to);

void namesFiles(std::string const &repertoire, std::vector<std::string> &fics);

#endif // TOOLS_H
