#!/bin/bash
# compilation 1.0

mkdir compilation

cd Lightbot/

g++ -c -m64 -pipe -std=c++11 -o ../compilation/main.o main.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/Application.o Application.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/Position.o Position.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/Grid.o Grid.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/Level.o Level.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/Menu.o Menu.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/Button.o Button.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/Robot.o Robot.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/Editor.o Editor.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/tools.o tools.cpp
g++ -c -m64 -pipe -std=c++11 -o ../compilation/MenuSelection.o MenuSelection.cpp

cd ../

g++ -m64 -o Lightbot_exe compilation/main.o compilation/Application.o compilation/Position.o compilation/Grid.o compilation/Level.o compilation/Menu.o compilation/Button.o compilation/Robot.o compilation/Editor.o compilation/tools.o compilation/MenuSelection.o -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system

rm -r compilation
